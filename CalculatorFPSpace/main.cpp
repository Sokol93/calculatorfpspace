/*
 * Author: Krzysztof Sokolowski
 * E-mail: sokolowski.krzysztof.1993@gmail.com
 */


#include <iostream>
#include "Calculator.h"
#include "MedianFunc.h"
#include "FibonacciSequenceFunc.h"

using namespace std;

int main()
{
	cout << "Before using this program please check READ_ME file!" << endl;
	Calculator &calculator = Calculator::get_instance();

	MedianFunc median_func;
	FibonacciNumberFunc fibonacci_number_func;

	calculator.add_extra_functions(&median_func);
	calculator.add_extra_functions(&fibonacci_number_func);
	// Can add new functions with just simple inheritance from CalcFunction

	string line;

	while(true)
	{
		getline(cin, line);

		if (line == "exit")
			break;

		calculator.calculate(line);
	}

	return 0;
}