#pragma once

#include <string>
#include <vector>

/**
* \brief Gets parameters list for function occurrence in expression
* \param expression Expression
* \return List of function parameters
*/
std::vector<double>& get_parameters(const std::string& expression);
/**
 * \brief Tries to parse from string to double
 * \param value_str Value in string
 * \param out_value Reference to save parsed value
 * \return True - if parse was successful 
 */
bool try_parse(const std::string& value_str, double& out_value);