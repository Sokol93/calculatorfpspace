#pragma once

#include <string>
#include "CustomExceptions.h"

/**
 * \brief Class indicates exception in expression
 */
class ExpressionException : public CustomExceptions
{
private:
	/**
	 * \brief Extract of expression where error happens
	 */
	std::string extract;

public:
	/**
	 * \brief Initializes exception message and extract from expression with error
	 * \param exception_message Exception message
	 * \param expression_extract Extract from expression where error occurs
	 */
	ExpressionException(const std::string& exception_message, const std::string& expression_extract = "")
		: CustomExceptions(exception_message), extract(expression_extract)
	{}

	//-------------------------------------------------------------------------------------------------------------

	/**
	 * \brief Gets expression extract
	 * \return Expression extract
	 */
	const std::string& get_expression_extract() const { return extract; }
};
