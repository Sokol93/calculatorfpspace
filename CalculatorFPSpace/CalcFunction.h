#pragma once

#include <string>
#include <vector>
#include <algorithm>

/**
 * \brief Abstract class for Calculator Functions
 */
class CalcFunction
{
protected:
	/**
	 * \brief Function name which will indicate it in expression
	 */
	std::string func_name;
	/**
	 * \brief Number of parameters of function. Value -1 means various number of parameters is allowed
	 */
	short param_number;

public:

	/**
	 * \brief Initializes function name and parameters number
	 * \param function_name Function name
	 * \param parameters_number Function parameters number
	 */
	CalcFunction(const std::string& function_name, const short parameters_number = -1)
		: func_name(function_name), param_number(parameters_number)
	{
		std::transform(func_name.begin(), func_name.end(), func_name.begin(), ::tolower);
	}
	/**
	 * \brief Default destructor
	 */
	virtual ~CalcFunction() = default;

	//-------------------------------------------------------------------------------------------------------------

	/**
	 * \brief Gets function name (always lower cases)
	 * \return Function name
	 */
	virtual const std::string& get_function_name() const { return func_name; }
	/**
	 * \brief Get function parameters number
	 * \return Parameters number (-1 means various number of parameters is allowed)
	 */
	virtual short get_parameters_number() const { return param_number; }

	//-------------------------------------------------------------------------------------------------------------

	/**
	 * \brief Calculate function for parameters
	 * \param parameters Parameters of function in string with brackets '{','}'
	 * \return Function value
	 */
	virtual double calculate_function(std::vector<double>& parameters) const = 0;
};