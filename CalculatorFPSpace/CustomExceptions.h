#pragma once

#include <string>

/**
 * \brief Custom exceptions class
 */
class CustomExceptions
{
protected:
	/**
	 * \brief Exception message
	 */
	std::string message;

public:
	/**
	 * \brief Initializes exception message
	 * \param exception_message Exception message
	 */
	CustomExceptions(const std::string& exception_message)
		: message(exception_message)
	{}
	/**
	* \brief Default destructor
	*/
	virtual ~CustomExceptions() = default;

	//-------------------------------------------------------------------------------------------------------------

	/**
	 * \brief Gets exception message
	 * \return Exception message
	 */
	virtual const std::string& get_message() const { return message; } 
};
