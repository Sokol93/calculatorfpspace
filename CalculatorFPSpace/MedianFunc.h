#pragma once

#include <algorithm>
#include "CalcFunction.h"
#include "ArgumentException.h"


/**
 * \brief Class representing Median function
 */
class MedianFunc : public CalcFunction
{
public:
	/**
	 * \brief Default constructor for MedianFunc - name: MED, parameters number: -1
	 */
	MedianFunc() 
		: CalcFunction("MED", -1)
	{}

	//-------------------------------------------------------------------------------------------------------------

	/**
	 * \brief Calculates median value of collection
	 * \param parameters List of values for median
	 * \return Median value of collection
	 */
	double calculate_function(std::vector<double>& parameters) const override
	{
		if (parameters.size() == 0)
		{
			throw ArgumentException("Parameter list for Median function can't be empty!", "parameters");
		}

		std::sort(parameters.begin(), parameters.end());

		const auto size = parameters.size();

		return (size % 2 == 0) ? ((parameters[size/2 - 1] + parameters[size/2])/2) : (parameters[size/2]);
	}
};

