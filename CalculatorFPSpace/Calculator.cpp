#include "Calculator.h"
#include <cctype>

using namespace std;


string Calculator::historical_data_path = "historical_data.txt";

map<string, int> Calculator::operators_priority = map<string, int>({
	{ "(", 0 },
	{ "+", 1 },
	{ "-", 1 },
	{ ")", 1 },
	{ "*", 2 },
	{ "/", 2 },
	{ "%", 2 },
	{ "^", 3 }
});

vector<char> Calculator::operators = vector<char>({ '(', '+', '-', ')', '*', '/', '%', '^', ',' });

list<string> Calculator::functions = list<string>({ "sin", "cos", "tg", "ctg", "exp", "abs", "log" });

vector<char> Calculator::left_associative = vector<char>({ '+', '-', '*', '/' });
//-------------------------------------------------------------------------------------------------------------
void Calculator::calculate(string expression)
{
	transform(expression.begin(), expression.end(), expression.begin(), ::tolower);

	if (expression.length() > 1)
	{
		try
		{
			auto parsed = rpn_parse(expression);
			actual_value = rpn_calculate(parsed);

			operations_history.push_back(expression);
			print(to_string(actual_value), "Value");
		}
		catch (ExpressionException e)
		{
			print(e.get_message() + " " + e.get_expression_extract(), "ERROR");
		}
		catch (ArgumentException e)
		{
			print(e.get_message() + " " + e.get_argument_name(), "ERROR");
		}
	}
	else
	{
		if (std::tolower(expression[0]) == 'c')
		{
			actual_value = 0;
			print(to_string(actual_value), "VALUE");
		}
		else
		{
			print("Invalid expression", "ERROR");
		}
	}
}
//-------------------------------------------------------------------------------------------------------------
Calculator::Calculator()
	: actual_value(0)
{
	fstream historical_data(historical_data_path, fstream::in);

	if (historical_data.good())
	{
		try
		{
			historical_data >> actual_value;

			string line;

			while (!historical_data.eof())
			{
				getline(historical_data, line);
				print(line, "H");
			}

			print(to_string(actual_value), "VALUE");
		}
		catch (...)
		{
			actual_value = 0;
		}
	}
}
//-------------------------------------------------------------------------------------------------------------
Calculator::~Calculator()
{
	fstream historical_data(historical_data_path, fstream::out | fstream::trunc);

	if(historical_data.good())
	{
		try
		{
			historical_data << actual_value << endl;

			for (const auto operation_line : operations_history)
				historical_data << operation_line << endl;
		}
		catch (...)
		{
			
		}
	}
}
//-------------------------------------------------------------------------------------------------------------
void Calculator::print(const std::string& text, const std::string& status)
{
	cout << status << ">> \t" << text << endl << endl;
}
//-------------------------------------------------------------------------------------------------------------
void Calculator::RPN_calculate_function_value(stack<string>& values, const string& func)
{
	double arg1, arg2;
	double value;

	if (func == "+")
	{
		if (values.size() < 2)
			throw ArgumentException("Not enough values for '+' operator!", "values");
		arg1 = stod(values.top());
		values.pop();
		arg2 = stod(values.top());
		values.pop();

		value = arg2 + arg1;
	}
	else if (func == "-")
	{
		if (values.size() < 2)
			throw ArgumentException("Not enough values for '-' operator!", "values");
		arg1 = stod(values.top());
		values.pop();
		arg2 = stod(values.top());
		values.pop();

		value = arg2 - arg1;
	}
	else if (func == "*")
	{
		if (values.size() < 2)
			throw ArgumentException("Not enough values for '*' operator!", "values");
		arg1 = stod(values.top());
		values.pop();
		arg2 = stod(values.top());
		values.pop();

		value = arg2 * arg1;
	}
	else if (func == "/")
	{
		if (values.size() < 2)
			throw ArgumentException("Not enough values for '/' operator!", "values");
		arg1 = stod(values.top());
		values.pop();
		arg2 = stod(values.top());
		values.pop();

		if (arg1 == 0)
			throw ArgumentException("Can't divide by 0!", "values");

		value = arg2 / arg1;
	}
	else if (func == "!")
	{
		if (values.size() < 1)
			throw ArgumentException("Not enough values for '!' operator!", "values");
		arg1 = stod(values.top());
		values.pop();

		value = -arg1;
	}
	else if (func == "^")
	{
		if (values.size() < 2)
			throw ArgumentException("Not enough values for '^' operator!", "values");
		arg1 = stod(values.top());
		values.pop();
		arg2 = stod(values.top());
		values.pop();

		if (arg1 == 0)
			throw ArgumentException("Can't divide by 0!", "values");

		value = pow(arg2, arg1);
	}
	else
	{
		for (CalcFunction* function : extra_functions)
		{
			auto name = function->get_function_name();

			if (function->get_function_name() == func)
			{
				vector<double> parameters;
				while (values.top() != ")")
				{
					parameters.push_back(stod(values.top()));
					values.pop();
				}

				values.pop(); // removes bracket indicating function end
				value = function->calculate_function(parameters);
				values.push(to_string(value));

				return;
			}
		}

		throw ExpressionException("Expression contains unknown elements");
	}

	values.push(to_string(value));
}
//-------------------------------------------------------------------------------------------------------------
list<string> Calculator::separate_elements(string expression)
{
	list<string> elements;

	size_t white_space_pos = expression.find(' ');

	// Remove white spaces
	while (white_space_pos != string::npos)
	{
		expression.erase(white_space_pos, 1);
		white_space_pos = expression.find(' ');
	}

	while (expression.length() > 0)
	{
		// If this is any operator
		if (find(operators.begin(), operators.end(), expression[0]) != operators.end())
		{
			elements.push_back(expression.substr(0, 1));
			expression.erase(expression.begin());
		}
		else // In other case load variable/function
		{
			auto end_iter = find_first_of(expression.begin(), expression.end(), operators.begin(), operators.end());
			elements.push_back(string(expression.begin(), end_iter));
			expression.erase(expression.begin(), end_iter);
		}
	}

	return elements;
}
//-------------------------------------------------------------------------------------------------------------
list<string> Calculator::rpn_parse(const string& expression)
{
	list<string> elements = separate_elements(expression);
	stack<string> my_stack;
	list<string> output;
	bool function_found = false;

	for (const string element : elements)
	{
		// Function
		if (find(functions.begin(), functions.end(), element) != functions.end())
		{
			my_stack.push(element);
			function_found = true;
		}
		// Value
		else if (element.length() >= 1 && find(operators.begin(), operators.end(), element[0]) == operators.end())
		{
			// If the value should be actual_value
			if (element == "a")
				output.push_back(to_string(actual_value));
			else
				output.push_back(element);
		}
		// Argument separator ','
		else if (element == ",")
		{
			while (!my_stack.empty() && my_stack.top() != "(")
			{
				output.push_back(my_stack.top());
				my_stack.pop();
			}

			// Closing '(' not found
			if (my_stack.empty())
				throw ExpressionException("Comma in wrong place or there is no closing bracket for function!");
		}
		// Opening bracket '('
		else if (element == "(")
		{
			my_stack.push(element);
			if (function_found)
			{
				function_found = false;
				output.push_back(")"); // This bracket will indicate end of function (needed for functions with unknown argument number)
			}
		}
		// Closing bracket ')'
		else if (element == ")")
		{
			while (!my_stack.empty() && my_stack.top() != "(")
			{
				output.push_back(my_stack.top());
				my_stack.pop();
			}

			if (my_stack.empty())
				throw ExpressionException("Brackets in wrong places!");

			my_stack.pop(); // removing opening bracket from stack
			if (!my_stack.empty() && find(functions.begin(), functions.end(), my_stack.top()) != functions.end())
			{
				output.push_back(my_stack.top());
				my_stack.pop();
			}
		}
		// Operator
		else if (find(operators.begin(), operators.end(), element[0]) != operators.end())
		{
			while (!my_stack.empty() &&
				((find(left_associative.begin(), left_associative.end(), element[0]) != left_associative.end() && operators_priority[element] <= operators_priority[my_stack.top()])
					||
					(find(left_associative.begin(), left_associative.end(), element[0]) == left_associative.end() && operators_priority[element] < operators_priority[my_stack.top()])))
			{
				output.push_back(my_stack.top());
				my_stack.pop();
			}
			my_stack.push(element);
		}
	}

	while (!my_stack.empty())
	{
		if (my_stack.top() == "(" || my_stack.top() == ")")
			throw ExpressionException("Brackets in wrong places!");

		output.push_back(my_stack.top());
		my_stack.pop();
	}

	return output;
}
//-------------------------------------------------------------------------------------------------------------
double Calculator::rpn_calculate(list<string>& rpn_expression)
{
	stack<string> values;
	double temp = 0;

	// Calculating with the left-to-right algorithm
	while(!rpn_expression.empty())
	{
		const string front = rpn_expression.front();

		if (front == ")" || try_parse(front, temp))
		{
			values.push(front);
		}
		else
		{
			RPN_calculate_function_value(values, front);
		}

		rpn_expression.pop_front();
	}

	return stod(values.top());
}

