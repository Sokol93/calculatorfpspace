#pragma once

#include <iostream>
#include <fstream>
#include "functions.h"
#include "ArgumentException.h"
#include "ExpressionException.h"
#include <list>
#include <stack>
#include <map>
#include <algorithm>
#include "CalcFunction.h"


/**
 * \brief Class modeling calculator functionality (singleton)
 */
class Calculator
{
	/**
	 * \brief Path to file with historical data
	 */
	static std::string historical_data_path;
	/**
	 * \brief Priority of operators for RPN
	 */
	static std::map<std::string, int> operators_priority;
	/**
	 * \brief Available operators
	 */
	static std::vector<char> operators;
	/**
	 * \brief Available functions. Can be extended by CalcFunction
	 */
	static std::list<std::string> functions;
	/**
	 * \brief Left associative operators for RPN
	 */
	static std::vector<char> left_associative;

	/**
	 * \brief Actual value of calculator
	 */
	double actual_value;
	/**
	 * \brief History of valid operations
	 */
	std::list<std::string> operations_history;
	/**
	 * \brief Extra functions for calculator defined as CalcFunction class
	 */
	std::list<CalcFunction*> extra_functions;
public:
	/**
	 * \brief Saves operations history to historical data file
	 */
	~Calculator();

	//-------------------------------------------------------------------------------------------------------------

	/**
	 * \brief Gets instance of calculator
	 * \return Instance of calculator
	 */
	static Calculator& get_instance()
	{
		static Calculator calculator;
		return calculator;
	}
	/**
	 * \brief Validates expression and calculates its value
	 * \param expression Expression to calculate
	 */
	void calculate(std::string expression);
	/**
	 * \brief Adds extra function to calculator
	 * \param function New calculator function
	 */
	void add_extra_functions(CalcFunction* function)
	{
		functions.push_back(function->get_function_name());
		extra_functions.push_back(function);
	}
private:
	/**
	* \brief Loads historical data if exists and initializes calculator Actual Value with data from this data.
	* Its private because of its singleton
	*/
	Calculator();

	// Because its singleton...
	Calculator(Calculator const &) = delete;
	void operator=(Calculator const &) = delete;
	/**
	 * \brief Formats text and prints to standard output
	 * \param text Text to print
	 */
	static void print(const std::string& text, const std::string& status = "");
	/**
	 * \brief Calculates value for function using values in stack. New value is at the top of the stack
	 * \param values Stack of values
	 * \param func Function which values will be calculated with
	 */
	void RPN_calculate_function_value(std::stack<std::string>& values, const std::string& func);
	/**
	 * \brief Separate elements in expression to list of single operators, values, functions
	 * \param expression Expression
	 * \return Expression as list of single operators, values, functions
	 */
	static std::list<std::string> separate_elements(std::string expression);
	/**
	 * \brief Parses expression to RPN (Reverse Polish Notation)
	 * \param expression Expression
	 * \return Expression in RPN
	 */
	std::list<std::string> rpn_parse(const std::string& expression);
	/**
	 * \brief Calculates RPN expression value
	 * \param rpn_expression Reverse Polish Notation expression
	 * \return Expression value
	 */
	double rpn_calculate(std::list<std::string>& rpn_expression);
};