#include "functions.h"

//-------------------------------------------------------------------------------------------------------------
std::vector<double>& get_parameters(const std::string& expression)
{
	size_t param_start = 0;
	size_t param_end = expression.length() - 1;

	std::vector<double> parameters;
	param_start += 1;
	const auto param_len = param_end;

	do
	{
		param_end = expression.find(',', param_start);

		if (param_end == std::string::npos)
			param_end = param_len;

		if (param_end - param_start > 0)
			parameters.push_back(std::stod(expression.substr(param_start, param_end - param_start)));

		param_start = param_end + 1;
	} while (param_end < param_len);

	return parameters;
}
//-------------------------------------------------------------------------------------------------------------
bool try_parse(const std::string& value_str, double& out_value)
{
	try
	{
		out_value = stod(value_str);
		return true;
	}
	catch (...)
	{
		return false;
	}
}