#pragma once

#include <string>
#include "CustomExceptions.h"

/**
* \brief Class indicates exception in argument
*/
class ArgumentException : public CustomExceptions
{
private:
	/**
	* \brief Argument name
	*/
	std::string arg_name;

public:
	/**
	* \brief Initializes exception message and argument name
	* \param exception_message Exception message
	* \param argument_name Argument name
	*/
	ArgumentException(const std::string& exception_message, const std::string& argument_name)
		: CustomExceptions(exception_message), arg_name(argument_name)
	{}

	//-------------------------------------------------------------------------------------------------------------

	/**
	* \brief Gets Argument name
	* \return Argument name
	*/
	const std::string& get_argument_name() const { return arg_name; }
};
