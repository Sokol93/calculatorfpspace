#pragma once

#include "CalcFunction.h"
#include "ArgumentException.h"


/**
* \brief Class representing function evaluating Fibonacci number
*/
class FibonacciNumberFunc : public CalcFunction
{
public:
	/**
	* \brief Default constructor for FibonacciNumberFunc - name: FIB, parameters number: 1
	*/
	FibonacciNumberFunc()
		: CalcFunction("FIB", 1)
	{}

	//-------------------------------------------------------------------------------------------------------------

	/**
	* \brief Calculates Fibonacci number
	* \param parameters Number in sequence [0, inf]
	* \return Fibonacci number
	*/
	double calculate_function(std::vector<double>& parameters) const override
	{
		if (parameters.size() != param_number)
		{
			throw ArgumentException("Only one parameter available for FibonacciNumberFunc!", "parameters");
		}

		if (parameters[0] < 0)
		{
			throw ArgumentException("Number in Fibonacci sequence must be greater or equal 0!", "parameters");
		}

		const unsigned int number_in_sequence = parameters[0];

		if (number_in_sequence == 0)
			return 0;
		if (number_in_sequence == 1)
			return 1;

		double first = 0, second = 1, next = 0;

		for (int i = 1; i < number_in_sequence; ++i)
		{
			next = first + second;
			first = second;
			second = next;
		}

		return next;
	}
};