var class_calc_function =
[
    [ "CalcFunction", "class_calc_function.html#a7e23e60f3b06b5b4df27ca10b47b8761", null ],
    [ "~CalcFunction", "class_calc_function.html#aa4ce826126734ba810276ca9f8e99652", null ],
    [ "calculate_function", "class_calc_function.html#a5d7bbce97e8b018226c00a53d67cb574", null ],
    [ "get_function_name", "class_calc_function.html#a42fc7a3506e4d94cdb2513acd7f95910", null ],
    [ "get_parameters_number", "class_calc_function.html#ada721a50f0173e994758fe7cb76a2e8d", null ],
    [ "func_name", "class_calc_function.html#adcf298eb07eb340900fb7877da0114bd", null ],
    [ "param_number", "class_calc_function.html#a8024b70e4bb12b8785ea71e7ee2fe535", null ]
];