var annotated_dup =
[
    [ "ArgumentException", "class_argument_exception.html", "class_argument_exception" ],
    [ "CalcFunction", "class_calc_function.html", "class_calc_function" ],
    [ "Calculator", "class_calculator.html", "class_calculator" ],
    [ "CustomExceptions", "class_custom_exceptions.html", "class_custom_exceptions" ],
    [ "ExpressionException", "class_expression_exception.html", "class_expression_exception" ],
    [ "FibonacciNumberFunc", "class_fibonacci_number_func.html", "class_fibonacci_number_func" ],
    [ "MedianFunc", "class_median_func.html", "class_median_func" ]
];