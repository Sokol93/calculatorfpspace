var hierarchy =
[
    [ "CalcFunction", "class_calc_function.html", [
      [ "FibonacciNumberFunc", "class_fibonacci_number_func.html", null ],
      [ "MedianFunc", "class_median_func.html", null ]
    ] ],
    [ "Calculator", "class_calculator.html", null ],
    [ "CustomExceptions", "class_custom_exceptions.html", [
      [ "ArgumentException", "class_argument_exception.html", null ],
      [ "ExpressionException", "class_expression_exception.html", null ]
    ] ]
];